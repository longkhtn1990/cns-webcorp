<?php
defined('BASEPATH') OR exit('No direct script access allowed');

/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	https://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There are three reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router which controller/method to use if those
| provided in the URL cannot be matched to a valid route.
|
|	$route['translate_uri_dashes'] = FALSE;
|
| This is not exactly a route, but allows you to automatically route
| controller and method names that contain dashes. '-' isn't a valid
| class or method name character, so it requires translation.
| When you set this option to TRUE, it will replace ALL dashes in the
| controller and method URI segments.
|
| Examples:	my-controller/index	-> my_controller/index
|		my-controller/my-method	-> my_controller/my_method
*/
$route['default_controller'] = 'home';
$route['404_override'] = '';
$route['translate_uri_dashes'] = FALSE;

//About
$route['introduce/(:any)'] = 'introduce/show/$1';

//Business
$route['business/(:any)'] = 'business/show/$1';

//Project
$route['project/(:any)'] = 'project/show/$1';
$route['project/detail/(:any)'] = 'project/showDetail/$1';

//Partner - Customer
$route['customer-partner'] = 'partner/show';

//News
$route['news-media'] = 'news/show';
$route['news-media/(:any)'] = 'news/showDetail/$1';

//////////////////////////////////////////////////ADMIN
$route['admin/main-info/(:any)/(:any)'] = 'admin/mainInfo/$1/$2';
#region MAIN SLIDE
$route['admin/main-banner/create/(:any)/(:any)'] = 'admin/mainBannerCreate/$1/$2';
$route['admin/main-banner/edit/(:any)/(:any)/(:any)'] = 'admin/mainBannerEdit/$1/$2/$3';
$route['admin/main-banner/detail/(:any)/(:any)/(:any)'] = 'admin/mainBannerDetail/$1/$2/$3';
$route['admin/main-banner/list/(:any)/(:any)'] = 'admin/mainBannerList/$1/$2';
$route['admin/main-banner/search'] = 'admin/mainBannerSearch';
#endregion

#region PROJECT
$route['admin/project/create/(:any)/(:any)'] = 'admin/projectCreate/$1/$2';
$route['admin/project/edit/(:any)/(:any)/(:any)'] = 'admin/projectEdit/$1/$2/$3';
$route['admin/project/detail/(:any)/(:any)/(:any)'] = 'admin/projectDetail/$1/$2/$3';
$route['admin/project/list/(:any)/(:any)'] = 'admin/projectList/$1/$2';
$route['admin/project/search'] = 'admin/projectSearch';
#endregion

#region PROJECT DETAIL
$route['admin/project-detail/create/(:any)/(:any)'] = 'admin/projectDetailCreate/$1/$2';
$route['admin/project-detail/edit/(:any)/(:any)/(:any)'] = 'admin/projectDetailEdit/$1/$2/$3';
$route['admin/project-detail/detail/(:any)/(:any)/(:any)'] = 'admin/projectDetailDetail/$1/$2/$3';
$route['admin/project-detail/list/(:any)/(:any)'] = 'admin/projectDetailList/$1/$2';
$route['admin/project-detail/search'] = 'admin/projectDetailSearch';
#endregion

#region BUSINESS
$route['admin/business/create/(:any)/(:any)'] = 'admin/businessCreate/$1/$2';
$route['admin/business/edit/(:any)/(:any)/(:any)'] = 'admin/businessEdit/$1/$2/$3';
$route['admin/business/detail/(:any)/(:any)/(:any)'] = 'admin/businessDetail/$1/$2/$3';
$route['admin/business/list/(:any)/(:any)'] = 'admin/businessList/$1/$2';
$route['admin/business/search'] = 'admin/businessSearch';
#endregion

#region NEWS
$route['admin/news/create/(:any)/(:any)'] = 'admin/newsCreate/$1/$2';
$route['admin/news/edit/(:any)/(:any)/(:any)'] = 'admin/newsEdit/$1/$2/$3';
$route['admin/news/detail/(:any)/(:any)/(:any)'] = 'admin/newsDetail/$1/$2/$3';
$route['admin/news/list/(:any)/(:any)'] = 'admin/newsList/$1/$2';
$route['admin/news/setting/(:any)/(:any)'] = 'admin/newsSetting/$1/$2';
$route['admin/news/search'] = 'admin/newsSearch';
#endregion

#region INTRODUCE
$route['admin/introduce/create/(:any)/(:any)'] = 'admin/introduceCreate/$1/$2';
$route['admin/introduce/edit/(:any)/(:any)/(:any)'] = 'admin/introduceEdit/$1/$2/$3';
$route['admin/introduce/detail/(:any)/(:any)/(:any)'] = 'admin/introduceDetail/$1/$2/$3';
$route['admin/introduce/list/(:any)/(:any)'] = 'admin/introduceList/$1/$2';
$route['admin/introduce/search'] = 'admin/introduceSearch';
#endregion

#region INTRODUCE ABOUT
$route['admin/introduceAbout/edit/(:any)/(:any)/(:any)'] = 'admin/introduceAboutEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT AWARDS
$route['admin/introduceAboutAwards/edit/(:any)/(:any)/(:any)'] = 'admin/introduceAboutAwardsEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT CORE
$route['admin/introduceAboutCore/edit/(:any)/(:any)/(:any)'] = 'admin/introduceAboutCoreEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT AWARDS ARTICLE
$route['admin/introduceAboutAwardsArticle/edit/(:any)/(:any)/(:any)'] = 'admin/introduceAboutAwardsArticleEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT CORE
$route['admin/introduceAboutCoreArticle/edit/(:any)/(:any)/(:any)'] = 'admin/introduceAboutCoreArticleEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT NETWORK
$route['admin/introduceAboutNetwork/edit/(:any)/(:any)/(:any)'] = 'admin/introduceAboutNetworkEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT SOCIAL
$route['admin/introduceAboutSocial/edit/(:any)/(:any)/(:any)'] = 'admin/introduceAboutSocialEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT SYSTEM MANAGEMENT
$route['admin/introduceAboutSystemManagement/edit/(:any)/(:any)/(:any)'] = 'admin/introduceAboutSystemManagementEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT MODEL CULTURE
$route['admin/introduceModelCulture/edit/(:any)/(:any)/(:any)'] = 'admin/introduceModelCultureEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT MODEL STORIES
$route['admin/introduceModelStories/edit/(:any)/(:any)/(:any)'] = 'admin/introduceModelStoriesEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT MODEL STORIES MEMBER
$route['admin/introduceModelStoriesMember/edit/(:any)/(:any)/(:any)'] = 'admin/introduceModelStoriesMemberEdit/$1/$2/$3';
#endregion

#region INTRODUCE ABOUT NEWS
$route['admin/introduceNews/edit/(:any)/(:any)/(:any)'] = 'admin/introduceNewsEdit/$1/$2/$3';
#endregion
////////////////////////////////////////////////////SAVE
#region MAIN SLIDE
$route['admin/main-banner/save'] = 'save/saveMainSlideImage';
$route['admin/main-banner/active-inactive'] = 'save/activeOrInactiveMainImage';
$route['admin/main-banner/approve-deny'] = 'save/approveOrDenyMainImage';
$route['admin/main-banner/delete'] = 'save/deleteMainImage';
#endregion

#region PROJECT
$route['admin/project/save'] = 'save/saveProject';
$route['admin/project/active-inactive'] = 'save/activeOrInactiveProject';
$route['admin/project/approve-deny'] = 'save/approveOrDenyProject';
$route['admin/project/delete'] = 'save/deleteProject';
#endregion

#region PROJECT
$route['admin/project-detail/save'] = 'save/saveProjectDetail';
$route['admin/project-detail/active-inactive'] = 'save/activeOrInactiveProjectDetail';
$route['admin/project-detail/approve-deny'] = 'save/approveOrDenyProjectDetail';
$route['admin/project-detail/delete'] = 'save/deleteProjectDetail';
#endregion

#region BUSINESS
$route['admin/business/save'] = 'save/saveBusiness';
$route['admin/business/active-inactive'] = 'save/activeOrInactiveBusiness';
$route['admin/business/approve-deny'] = 'save/approveOrDenyBusiness';
$route['admin/business/delete'] = 'save/deleteBusiness';
#endregion

#region NEWS
$route['admin/news/save'] = 'save/saveNews';
$route['admin/news/active-inactive'] = 'save/activeOrInactiveNews';
$route['admin/news/approve-deny'] = 'save/approveOrDenyNews';
$route['admin/news/delete'] = 'save/deleteNews';
#endregion

#region INTRODUCE
$route['admin/introduce/save'] = 'save/saveIntroduce';
$route['admin/introduce/active-inactive'] = 'save/activeOrInactiveIntroduce';
$route['admin/introduce/approve-deny'] = 'save/approveOrDenyIntroduce';
$route['admin/introduce/delete'] = 'save/deleteIntroduce';
#endregion

#region INTRODUCE ABOUT
$route['admin/introduceAbout/save'] = 'save/saveIntroduceAbout';
#endregion

#region INTRODUCE ABOUT AWARDS
$route['admin/introduceAboutAwards/save'] = 'save/saveIntroduceAboutAwards';
#endregion

#region INTRODUCE ABOUT CORE
$route['admin/introduceAboutCore/save'] = 'save/saveIntroduceAboutCore';
#endregion

#region INTRODUCE ABOUT AWARDS ARTICLE
$route['admin/introduceAboutAwardsArticle/save'] = 'save/saveIntroduceAboutAwardsArticle';
#endregion

#region INTRODUCE ABOUT CORE ARTICLE
$route['admin/introduceAboutCoreArticle/save'] = 'save/saveIntroduceAboutCoreArticle';
#endregion

#region INTRODUCE ABOUT NETWORK
$route['admin/introduceAboutNetwork/save'] = 'save/saveIntroduceAboutNetwork';
#endregion

#region INTRODUCE ABOUT SOCIAL
$route['admin/introduceAboutSocial/save'] = 'save/saveIntroduceAboutSocial';
#endregion

#region INTRODUCE ABOUT SYSTEM MANAGEMENT
$route['admin/introduceAboutSystemManagement/save'] = 'save/saveIntroduceAboutSystemManagement';
#endregion

#region INTRODUCE MODEL CULTURE
$route['admin/introduceModelCulture/save'] = 'save/saveIntroduceModelCulture';
#endregion

#region INTRODUCE MODEL STORIES
$route['admin/introduceModelStories/save'] = 'save/saveIntroduceModelStories';
#endregion

#region INTRODUCE MODEL STORIES MEMBER
$route['admin/introduceModelStoriesMember/save'] = 'save/saveIntroduceModelStoriesMember';
#endregion

#region INTRODUCE NEWS
$route['admin/introduceNews/save'] = 'save/saveIntroduceNews';
#endregion














