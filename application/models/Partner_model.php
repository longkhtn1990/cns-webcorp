<?php
class Partner_model extends CI_Model
{
    public function get_data_partner_banner_display()
    {
        $sql = "SELECT main_menu.*
                FROM main_menu
                WHERE status = 'active' AND type = 'customer-partner' ";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_partner_global_display()
    {
        $sql = "SELECT partner_global.*
                FROM partner_global
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND status_approve = 'approved'
                ORDER BY date_display DESC";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
}