<?php
class News_model extends CI_Model
{
    public function get_list_category_display(){
        $sql = "SELECT * FROM news_setting";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_latest_news_display()
    {
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type IN ('company','market','general','newspaper')
                    AND status_approve = 'approved'
                ORDER BY date_display DESC";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_company_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'company'
                    AND status_approve = 'approved'
                ORDER BY date_display DESC
                LIMIT 6";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_market_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'market'
                    AND status_approve = 'approved'
                ORDER BY date_display DESC
                LIMIT 6";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_general_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'general'
                    AND status_approve = 'approved'
                ORDER BY date_display DESC
                LIMIT 6";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_newspaper_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND type = 'newspaper'
                    AND status_approve = 'approved'
                ORDER BY date_display DESC
                LIMIT 6";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    public function get_data_highlights_news_display(){
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND highlights = 1
                    AND type IN ('company','market','general','newspaper')
                    AND status_approve = 'approved'
                ORDER BY date_display DESC
                LIMIT 4";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }

    /*public function get_data_news_content_display($alias)
    {
        $sql = "SELECT news.*
                FROM news
                WHERE deleted = 0
                    AND date_display <= '" . date('Y-m-d') . "'
                    AND status = 'active'
                    AND status_approve = 'approved'
                    AND alias = '".$alias."'";
        $result = $this->db->query($sql)->row_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }*/

    /////////////////////////// ADMIN
    public function get_data_news_with_id($id = '')
    {
        if ($id != '') {
            $sql = "SELECT *
                    FROM news
                    WHERE id = '" . $id . "'";
            $result = $this->db->query($sql)->row_array();
            if ($result != false && count($result) > 0) {
                return $result;
            }
        }
        return false;
    }

    public function get_data_news_all($title = '',$status = '',$status_approve = '')
    {
        $sql = "SELECT *
                FROM news ";
        $where = ' WHERE deleted = 0 ';
        if($title != ''){
            $where .= ' AND ( title LIKE "%'.$title.'%" OR title_vn LIKE "%'.$title.'%") ';
        }
        if($status != ''){
            $where .= ' AND status = "'.$status.'" ';
        }
        if($status_approve != ''){
            $where .= ' AND status_approve = "'.$status_approve.'" ';
        }
        if($where != ''){
            $sql = $sql. trim(trim($where),"AND");
        }

        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }

        return false;
    }

    public function save_news($data = array())
    {
        if (count($data) > 0) {
            unset($data['id']);
            unset($data['date_approved']);
            if ($this->db->insert('news', $data)) {
                $insert_id = $this->db->insert_id();
                return $insert_id;
            }
        }
        return false;
    }

    public function update_news($id = '', $data = array())
    {
        if ($id != '' && count($data) > 0) {
            unset($data['id']);
            $this->db->where('id', 1)->update('news', $data);
            return true;
        }
        return false;
    }

    public function get_all_news_setting(){
        $sql = "SELECT news_setting.*
                FROM news_setting";
        $result = $this->db->query($sql)->result_array();
        if ($result != false && count($result) > 0) {
            return $result;
        }
        return false;
    }
}