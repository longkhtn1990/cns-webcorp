<section class="banner about-banner">
    <div class="banner-inner" style="background-image: url(<?php echo $banner['image']; ?>);">
        <div class="container banner-text">
            <div class="banner-text-inner">
                <h2 class="section-title">
                    <?php echo $banner['title']; ?>
                </h2>
            </div>
        </div>
    </div>
</section>