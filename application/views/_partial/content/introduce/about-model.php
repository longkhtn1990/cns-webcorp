<div id="introduce-<?php echo $currentPage != '' ? $currentPage : 'about-model'; ?>" class="introduce-content">
    <?php if ($content['culture']['title'] != ''): ?>
        <section class="section-block culture-section">
            <div class="container">
                <div class="section-header">
                    <h2 class="section-title"><?php echo $content['culture']['title'] != '' ? $content['culture']['title'] : ''; ?></h2>
                </div>
                <div class="section-content">
                    <div class="col-md-8">
                        <p><?php echo $content['culture']['content'] != '' ? $content['culture']['content'] : ''; ?></p>
                    </div>
                </div>
            </div>
        </section>
    <?php endif; ?>
    <?php if ($content['stories']['title'] != ''): ?>
    <section class="section-block stories-section">
        <div class="container">
            <div class="section-header">
                <h2 class="section-title"><?php echo $content['stories']['title']; ?></h2>
            </div>
            <div class="section-content">
                <div class="row">
                    <div class="col-md-push-6 col-md-6 awards-list-block memeberListSlider">
                        <?php if (count($content['stories']['member']) > 0): ?>
                        <?php $memberRow = 0; ?>
                        <?php for ($i = 0; $i < count($content['stories']['member']); $i++): ?>
                            <?php if ($memberRow == 0): ?>
                                <div class="row">
                            <?php endif; ?>

                            <div class="col-xs-6 member-item">
                                <a class="active" href="javascript:void(0)" onclick="loadInfoMember(this)">
                                    <figure class="article-img member-item-img">
                                        <img src="<?php echo $content['stories']['member'][$i]['image']; ?>"
                                             alt="">
                                    </figure>
                                    <input type="hidden" class="member-name-hidden"
                                           value="<?php echo $content['stories']['member'][$i]['name']; ?>"/>
                                    <input type="hidden" class="member-position-hidden"
                                           value="<?php echo $content['stories']['member'][$i]['position']; ?>"/>
                                    <input type="hidden" class="member-info-hidden"
                                           value="<?php echo $content['stories']['member'][$i]['info']; ?>"/>
                                </a>
                            </div>

                            <?php $memberRow++; ?>
                            <?php if ($memberRow > 3): ?>
                                </div>
                                <?php $memberRow = 0; ?>
                            <?php endif; ?>
                        <?php endfor; ?>
                        <?php if ($memberRow != 0): ?>
                            </div>
                        <?php endif; ?>
                    <?php endif; ?>
                </div>
                <article class="col-md-pull-6 col-md-6 article-item member-detail-block">
                    <?php if ($content['stories']['member'][0]['image'] != ''): ?>
                        <div class="article-item-inner">
                            <figure id="member-detail-img" class="article-img member-detail-img">
                                <img src="<?php echo $content['stories']['member'][0]['image']; ?>" alt="">
                            </figure>
                            <div class="article-item-detail">
                                <h3 class="article-item-title">
                                    <span id="member-name" class="member-name"><?php echo $content['stories']['member'][0]['name']; ?></span>
                                    <span class="mb-icon"> - </span>
                                    <span id="member-position" class="member-position"><?php echo $content['stories']['member'][0]['position']; ?></span>
                                </h3>
                                <div id="member-info" class="short-info">
                                    <p><?php echo $content['stories']['member'][0]['info']; ?></p>
                                </div>
                            </div>
                        </div>
                    <?php endif; ?>
                </article>
            </div>
        </div>
</div>
</section>
<?php endif; ?>
</div>