<div id="introduce-<?php echo $currentPage != '' ? $currentPage : 'about-news'; ?>" class="introduce-content">
    <?php if ($content['news']['title'] != ''): ?>
        <section class="section-block business-section">
            <div class="container">
                <div class="section-header">
                    <h2 class="section-title"><?php echo $content['news']['title'] != '' ? $content['news']['title'] : ''; ?></h2>
                </div>
                <div class="section-content">
                    <?php echo $content['news']['content'] != '' ? $content['news']['content'] : ''; ?>
                </div>
            </div>
        </section>
    <?php endif; ?>
</div>