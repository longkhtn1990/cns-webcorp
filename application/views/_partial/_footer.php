<footer>
    <section class="footer-links-section">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="footer-links-col">
                        <?php if (isset($mainFooter['col-1'])): ?>
                            <h4 class="list-title-item"><?php echo $mainFooter['col-1']['text'] != '' ? $mainFooter['col-1']['text'] : ''; ?></h4>
                            <ul class="list-group footer-links-list">
                                <?php if (count($mainFooter['col-1']['item']) > 0): ?>
                                    <?php for ($i = 0; $i < count($mainFooter['col-1']['item']); $i++): ?>
                                        <li>
                                            <a href="<?php echo $mainFooter['col-1']['item'][$i]['link'] != '' ? $mainFooter['col-1']['item'][$i]['link'] : ''; ?>"><?php echo $mainFooter['col-1']['item'][$i]['text'] != '' ? $mainFooter['col-1']['item'][$i]['text'] : ''; ?></a>
                                        </li>
                                    <?php endfor; ?>
                                <?php endif; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="footer-links-col">
                        <?php if (isset($mainFooter['col-2'])): ?>
                            <h4 class="list-title-item"><?php echo $mainFooter['col-2']['text'] != '' ? $mainFooter['col-2']['text'] : ''; ?></h4>
                            <ul class="list-group footer-links-list">
                                <?php if (count($mainFooter['col-2']['item']) > 0): ?>
                                    <?php for ($i = 0; $i < count($mainFooter['col-2']['item']); $i++): ?>
                                        <li>
                                            <a href="<?php echo $mainFooter['col-2']['item'][$i]['link'] != '' ? $mainFooter['col-2']['item'][$i]['link'] : ''; ?>"><?php echo $mainFooter['col-2']['item'][$i]['text'] != '' ? $mainFooter['col-2']['item'][$i]['text'] : ''; ?></a>
                                        </li>
                                    <?php endfor; ?>
                                <?php endif; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="footer-links-col">
                        <?php if (isset($mainFooter['col-3'])): ?>
                            <h4 class="list-title-item"><?php echo $mainFooter['col-3']['text'] != '' ? $mainFooter['col-3']['text'] : ''; ?></h4>
                            <ul class="list-group footer-links-list">
                                <?php if (count($mainFooter['col-3']['item']) > 0): ?>
                                    <?php for ($i = 0; $i < count($mainFooter['col-3']['item']); $i++): ?>
                                        <li>
                                            <a href="<?php echo $mainFooter['col-3']['item'][$i]['link'] != '' ? $mainFooter['col-3']['item'][$i]['link'] : ''; ?>"><?php echo $mainFooter['col-3']['item'][$i]['text'] != '' ? $mainFooter['col-3']['item'][$i]['text'] : ''; ?></a>
                                        </li>
                                    <?php endfor; ?>
                                <?php endif; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="footer-links-col">
                        <?php if (isset($mainFooter['col-4'])): ?>
                            <h4 class="list-title-item"><?php echo $mainFooter['col-4']['text'] != '' ? $mainFooter['col-4']['text'] : ''; ?></h4>
                            <ul class="list-group footer-links-list">
                                <?php if (count($mainFooter['col-4']['item']) > 0): ?>
                                    <?php for ($i = 0; $i < count($mainFooter['col-4']['item']); $i++): ?>
                                        <li>
                                            <a href="<?php echo $mainFooter['col-4']['item'][$i]['link'] != '' ? $mainFooter['col-4']['item'][$i]['link'] : ''; ?>"><?php echo $mainFooter['col-4']['item'][$i]['text'] != '' ? $mainFooter['col-4']['item'][$i]['text'] : ''; ?></a>
                                        </li>
                                    <?php endfor; ?>
                                <?php endif; ?>
                            </ul>
                        <?php endif; ?>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6 col-md-4 col-lg-2">
                    <div class="footer-links-col">
                        <h4 class="list-title-item"><?php echo $mainFooter['connecting']['title'] != '' ? $mainFooter['connecting']['title'] : 'Kết nối với chúng tôi'; ?></h4>
                        <ul class="list-group social-group">
                            <?php if ($mainFooter['connecting']['facebook'] != ''): ?>
                                <li>
                                    <a href="<?php echo $mainFooter['connecting']['facebook'] != '' ? $mainFooter['connecting']['facebook'] : 'Kết nối với chúng tôi'; ?>"
                                       target="_blank">
                                        <i class="fa fa-facebook-official" aria-hidden="true"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                            <?php if ($mainFooter['connecting']['youtube'] != ''): ?>
                            <li>
                                <a href="<?php echo $mainFooter['connecting']['youtube'] != '' ? $mainFooter['connecting']['youtube'] : ''; ?>"
                                   target="_blank">
                                    <i class="fa fa-youtube-play" aria-hidden="true"></i>
                                </a>
                            </li>
                            <?php endif; ?>
                            <?php if ($mainFooter['connecting']['googlePlus'] != ''): ?>
                                <li>
                                    <a href="<?php echo $mainFooter['connecting']['googlePlus'] != '' ? $mainFooter['connecting']['googlePlus'] : ''; ?>"
                                       target="_blank">
                                        <i class="fa fa-google-plus" aria-hidden="true"></i>
                                    </a>
                                </li>
                            <?php endif; ?>
                        </ul>
                        <ul class="list-group visited-group">
                            <li>
                                <p>
                                    <?php echo $mainFooter['connecting']['totalView']['text'] != '' ? $mainFooter['connecting']['totalView']['text'] : 'Tổng lượt truy cập'; ?>: <span class="visited-number"><?php echo $mainFooter['connecting']['totalView']['number'] != '' ? $mainFooter['connecting']['totalView']['number'] : '0'; ?></span> <?php echo $mainFooter['connecting']['totalView']['unit'] != '' ? $mainFooter['connecting']['totalView']['unit'] : 'lượt'; ?>
                                </p>
                            </li>
                            <li>
                                <p>
                                    <?php echo $mainFooter['connecting']['weekView']['text'] != '' ? $mainFooter['connecting']['weekView']['text'] : 'Lượt truy cập trong tuần'; ?>: <span class="visited-number"><?php echo $mainFooter['connecting']['weekView']['number'] != '' ? $mainFooter['connecting']['weekView']['number'] : '0'; ?></span><?php echo $mainFooter['connecting']['weekView']['unit'] != '' ? $mainFooter['connecting']['weekView']['unit'] : 'lượt'; ?>
                                </p>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <div class="container footer-block">
        <nav class="footer-bottom-nav">
            <div class="col-md-1 footer-logo">
                <a href="<?php echo $link != '' ? $link : base_url(); ?>">
                    <img src="<?php echo $logo != '' ? $logo : base_url().'/assets/images/logo.png'; ?>" alt="CNS">
                </a>
            </div>
            <p class="copy-right">
                <?php echo $mainFooter['copyright']['pre-name'] != '' ? $mainFooter['copyright']['pre-name'] : 'Ủy ban Nhân dân TP.Hồ Chí Minh'; ?>
                <br>
                <?php echo $mainFooter['copyright']['name'] != '' ? $mainFooter['copyright']['name'] : 'Tổng Công ty Công Nghiệp Sài Gòn - Trách nhiệm hữu hạn Một Thành Viên'; ?>
            </p>
            <ul class="nav navbar-nav footer-menu-block">
                <li>
                    <a href="<?php echo $mainFooter['career-job']['link'] != '' ? $mainFooter['career-job']['link'] : 'javascript:void(0)'; ?>"><?php echo $mainFooter['career-job']['text'] != '' ? $mainFooter['career-job']['text'] : 'Tuyển dụng'; ?></a>
                </li>
                <li>
                    <a href="<?php echo $mainFooter['customer-contact']['link'] != '' ? $mainFooter['customer-contact']['link'] : 'javascript:void(0)'; ?>"><?php echo $mainFooter['customer-contact']['text'] != '' ? $mainFooter['customer-contact']['text'] : 'Liên hệ'; ?></a>
                </li>
            </ul>
        </nav>
    </div>
    <a class="back-top-btn" href="#"></a>
</footer>


<!--modal-->


<!--script-->
<script src="<?php echo base_url(); ?>assets/js/lib/jquery.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/lib/bootstrap.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/slick.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery.fitvids.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery.slimscroll.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/waypoints.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery.counterup.min.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/plugin/jquery.raty.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/script.js" type="text/javascript"></script>
<script src="<?php echo base_url(); ?>assets/js/introduce.js" type="text/javascript"></script>


<!--Start of Tawk.to Script-->
<script type="text/javascript">
    var Tawk_API = Tawk_API || {}, Tawk_LoadStart = new Date();
    (function () {
        var s1 = document.createElement("script"), s0 = document.getElementsByTagName("script")[0];
        s1.async = true;
        s1.src = 'https://embed.tawk.to/597874340d1bb37f1f7a5e96/default';
        s1.charset = 'UTF-8';
        s1.setAttribute('crossorigin', '*');
        s0.parentNode.insertBefore(s1, s0);
    })();
</script>
<!--End of Tawk.to Script-->