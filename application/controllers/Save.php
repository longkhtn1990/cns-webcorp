<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Save extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL
        $this->load->model('main_model');
        $this->load->model('project_model');
        $this->load->model('business_model');
        $this->load->model('introduce_model');
        $this->load->model('introduce_about_model');
        $this->load->model('introduce_about_awards_model');
        $this->load->model('introduce_about_awards_article_model');
        $this->load->model('introduce_about_core_model');
        $this->load->model('introduce_about_model');
//        $this->load->model('introduce_about_core_article_model');
//        $this->load->model('introduce_about_network_model');
        $this->load->model('introduce_about_social_model');
        $this->load->model('introduce_about_system_management_model');
        $this->load->model('introduce_model_culture_model');
        $this->load->model('introduce_model_stories_model');
//        $this->load->model('introduce_about_stories_model');
//        $this->load->model('introduce_about_stories_member_model');
//        $this->load->model('introduce_about_new_model');
//        $this->load->model('news_model');
        //LIBRARY
        $this->load->library('data');
        $this->load->library('common');
        $this->load->library('utils');
    }

    public function mainInfo($selectLanguage,$user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }
        $data = array(
            'domain' => 'http://webcorp.dev/',
            'language' => $this->lang->line('language')
        );
        $this->load->view('admin/main_info', $data);
    }


    #region MAIN SLIDE
    public function saveMainSlideImage()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,11)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $banner_slide_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->main_model->update_banner_slide_image($banner_slide_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $banner_slide_id = $this->main_model->save_banner_slide_image($data);
            }

            if($banner_slide_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'banner-'.$banner_slide_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'banner-'.$banner_slide_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->main_model->update_banner_slide_image($banner_slide_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $banner_slide_id));die();
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveMainImage(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->main_model->update_banner_slide_image($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyMainImage(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approve' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status_approve']
            );
            $this->main_model->update_banner_slide_image($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteMainImage(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->main_model->update_banner_slide_image($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region PROJECT
    public function saveProject()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,8)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->project_model->update_project($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->project_model->save_project($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'project-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'project-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->project_model->update_project($project_id,array('banner_image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveProject(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->project_model->update_project($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyProject(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approve' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status_approve']
            );
            $this->project_model->update_project($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteProject(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->project_model->update_project($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region PROJECT DETAIL
    public function saveProjectDetail()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,15)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->project_model->update_project_detail($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->project_model->save_project_detail($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'project-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'project-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->project_model->update_project_detail($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveProjectDetail(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->project_model->update_project_detail($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyProjectDetail(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approve' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status_approve']
            );
            $this->project_model->update_project_detail($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteProjectDetail(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->project_model->update_project_detail($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region BUSINESS
    public function saveBusiness()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,9)] = $parameter_value;
            }
            $banner_image_value = $data['banner_image_file_value'];
            $banner_image_type = $data['banner_image_file_type'];
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['banner_image_file_value']);
            unset($data['banner_image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->business_model->update_business($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->business_model->save_business($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'business-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'business-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->business_model->update_business($project_id,array('image' => $link_image));
            }
            if($project_id != '' && $banner_image_value != null && $banner_image_type != null){
                if($banner_image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $banner_image_value);
                    $image_name = 'business-banner-'.$project_id.'.png' ;
                }else if($banner_image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $banner_image_value);
                    $image_name = 'business-banner-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->business_model->update_business($project_id,array('banner_image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveBusiness(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->business_model->update_business($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyBusiness(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approve' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status_approve']
            );
            $this->business_model->update_business($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteBusiness(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->business_model->update_business($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region News
    public function saveNews()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,5)] = $parameter_value;
            }
            $banner_image_value = $data['banner_image_file_value'];
            $banner_image_type = $data['banner_image_file_type'];
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['banner_image_file_value']);
            unset($data['banner_image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->news_model->update_business($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->news_model->save_news($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'news-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'news-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->news_model->update_news($project_id,array('image' => $link_image));
            }
            if($project_id != '' && $banner_image_value != null && $banner_image_type != null){
                if($banner_image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $banner_image_value);
                    $image_name = 'news-banner-'.$project_id.'.png' ;
                }else if($banner_image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $banner_image_value);
                    $image_name = 'news-banner-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->news_model->update_news($project_id,array('banner_image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveNews(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->business_model->update_business($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyNews(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approve' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status_approve']
            );
            $this->business_model->update_business($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteNews(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->business_model->update_business($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region introduce
    public function saveIntroduce()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,10)] = $parameter_value;
            }
            $image_value = $data['image_file_value'];
            $image_type = $data['image_file_type'];
            $currentUser = $data['current_user'];
            unset($data['image_file_value']);
            unset($data['image_file_type']);
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_model->update_introduce($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_model->save_introduce($data);
            }

            if($project_id != '' && $image_type != null && $image_value != null){
                if($image_type == 'image/png'){
                    $image = str_replace('data:image/png;base64,', '', $image_value);
                    $image_name = 'introduce-'.$project_id.'.png' ;
                }else if($image_type == 'image/jpeg'){
                    $image = str_replace('data:image/jpeg;base64,', '', $image_value);
                    $image_name = 'introduce-'.$project_id.'.jpg' ;
                }else{
                    $image = null;
                    $image_name = null;
                }
                $img = str_replace(' ', '+', $image);
                $url_save = APPPATH . '/../assets/images/upload/' . $image_name;
                $link_image = 'assets/images/upload/' . $image_name;

                $this->utils->saveImage($img,$url_save);
                $this->introduce_model->update_introduce($project_id,array('image' => $link_image));
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }

    public function activeOrInactiveIntroduce(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'status' => $parameters['status']
            );
            $this->introduce_model->update_introduce($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function approveOrDenyIntroduce(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'date_approve' => date('Y-m-d H:i:s'),
                'approver' => $currentUser,
                'status_approve' => $parameters['status_approve']
            );
            $this->introduce_model->update_introduce($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            return;
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }

    public function deleteIntroduce(){
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $recordId = $parameters['record_id'];
        if($recordId != ''){
            $data = array(
                'modified_by' => $currentUser,
                'date_modified' => date('Y-m-d H:i:s'),
                'deleted' => 1
            );
            $this->introduce_model->update_introduce($recordId,$data);
            echo json_encode(array('error_code' => 1,'record' => $recordId));
            die();
        }
        echo json_encode(array('error_code' => 2,'record' => null));
    }
    #endregion

    #region introduce about
    public function saveIntroduceAbout()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,16)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_model->update_introduce_about($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_model->save_introduce_about($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion saveIntroduceAbout

    #region introduce about awards
    public function saveIntroduceAboutAwards()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,23)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_awards_model->update_introduce_about_awards($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_awards_model->save_introduce_about_awards($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion saveIntroduceAbout

    #region introduce about awards article
    public function saveIntroduceAboutAwardsArticle()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,31)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_awards_aritcle_model->update_introduce_about_awards_aritcle($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_awards_aritcle_model->save_introduce_about_awards_aritcle($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion saveIntroduceAbout

    #region introduce about core
    public function saveIntroduceAboutCore()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,21)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_core_model->update_introduce_about_core($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_core_model->save_introduce_about_core($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion saveIntroduceAbout

    #region introduce about social
    public function saveIntroduceAboutSocial()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,23)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_social_model->update_introduce_about_social($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_social_model->save_introduce_about_social($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion saveIntroduceAbout

    #region introduce about system management
    public function saveIntroduceAboutSystemManagement()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,34)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_about_system_management_model->update_introduce_about_system_management($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_about_system_management_model->save_introduce_about_system_management($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion saveIntroduceAbout

    #region introduce model culture
    public function saveIntroduceModelCulture()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,24)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_model_culture_model->update_introduce_model_culture($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_model_culture_model->save_introduce_model_culture($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion saveIntroduceAbout

    #region introduce model stories
    public function saveIntroduceModelStories()
    {
        $parameters = $this->input->post('parameters');
        $parameters = $this->utils->getFieldData($parameters,'value','name');
        if(count($parameters) > 0){
            $data = array();
            foreach($parameters AS $parameter_key =>  $parameter_value){
                $data[substr($parameter_key,24)] = $parameter_value;
            }
            $currentUser = $data['current_user'];
            unset($data['current_user']);
            unset($data['current_language']);
            $data['modified_by'] = $currentUser;
            $data['date_modified'] = date('Y-m-d H:i:s');
            $data['date_display'] = $this->utils->convertDateDB($data['date_display']);
            if($data['id'] != '' && $data['id'] != null){
                $project_id = $data['id'];
                if($data['date_approved'] == null || $data['date_approved'] == '') {
                    if ($data['status_approve'] == 'approved' || $data['status_approve'] == 'denied') {
                        $data['date_approved'] = date('Y-m-d H:i:s');
                    }
                }
                $this->introduce_model_stories_model->update_introduce_model_stories($project_id,$data);
            }else{
                $data['created_by'] = $currentUser;
                $data['date_entered'] = date('Y-m-d H:i:s');
                $project_id = $this->introduce_model_stories_model->save_introduce_model_stories($data);
            }
            echo json_encode(array('error_code' => 1,'record' => $project_id));
        }else{
            echo json_encode(array('error_code' => 2,'record' => null));
        }
    }
    #endregion saveIntroduceAbout
}
