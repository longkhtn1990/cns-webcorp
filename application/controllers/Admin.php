<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller
{
    private $domain = '';
    private $list_status_option = array(
        '' => '',
        'draft' => 'Draft',
        'active' => 'Active',
        'inactive' => 'Inactive'
    );
    private $list_status_approve_option = array(
        '' => '',
        'need_approve' => 'Need Approve',
        'approved' => 'Approved',
        'denied' => 'Denied'
    );
    private $list_position_business = array(
        '' => '',
        '1' => '1',
        '2' => '2',
        '3' => '3',
        '4' => '4'
    );
    private $list_type_news = array(
        '' => '',
        'company' => 'News Company',
        'market' => 'News Market',
        'general' => 'News General',
        'newspaper' => 'Newspaper'
    );
    private $list_highlights = array(
        '0' => 'None',
        '1' => 'Highlights'
    );


    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL
        $this->load->model('main_model');
        $this->load->model('project_model');
        $this->load->model('business_model');
        $this->load->model('introduce_model');
        $this->load->model('introduce_about_model');
        $this->load->model('introduce_about_awards_model');
        $this->load->model('introduce_about_awards_article_model');
        $this->load->model('introduce_about_core_model');
//        $this->load->model('introduce_about_core_article_model');
//        $this->load->model('introduce_about_network_model');
        $this->load->model('introduce_about_social_model');
        $this->load->model('introduce_about_system_management_model');
        $this->load->model('introduce_model_culture_model');
        $this->load->model('introduce_model_stories_model');
//        $this->load->model('introduce_about_stories_model');
//        $this->load->model('introduce_about_stories_member_model');
//        $this->load->model('introduce_about_new_model');
//        $this->load->model('news_model');
        //LIBRARY
        $this->load->library('data');
        $this->load->library('common');
        $this->load->library('utils');
        $info = $this->main_model->get_data_display();
        $this->domain = $info[0]['base_domain'];
    }

    public function mainInfo($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language')
        );
        $this->load->view('admin/main_info', $data);
    }


    #region MAIN SLIDE
    public function mainBannerCreate($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied'))
            )
        );
        $this->load->view('admin/banner_slide/create', $data);
    }

    public function mainBannerEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->main_model->get_data_banner_slide_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/banner_slide/create', $data);
    }

    public function mainBannerDetail($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->main_model->get_data_banner_slide_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/banner_slide/detail', $data);
    }

    public function mainBannerList($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/banner_slide/list', $data);
    }

    public function mainBannerSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->main_model->get_data_banner_slide_all($title, $status, $status_approve);
        if (count($dataSlide) > 0) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\''.$currentUser.'\',\''.$currentLanguage.'\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteBannerSlideImage(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region PROJECT
    public function projectCreate($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied'))
            )
        );
        $this->load->view('admin/project/create', $data);
    }

    public function projectEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->project_model->get_data_project_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/project/create', $data);
    }

    public function projectDetail($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->project_model->get_data_project_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/project/detail', $data);
    }

    public function projectList($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/project/list', $data);
    }

    public function projectSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->project_model->get_data_project_all($title, $status, $status_approve);
        if (count($dataSlide) > 0) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'project\',\''.$currentUser.'\',\''.$currentLanguage.'\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteBannerSlideImage(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['banner_image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region PROJECT DETAIL
    public function projectDetailCreate($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $project = $this->project_model->get_data_project_all();
        $projectData = $this->utils->getFieldData($project,'title','id');
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_project' => $this->common->generateSelectOption($projectData)
            )
        );
        $this->load->view('admin/project_detail/create', $data);
    }

    public function projectDetailEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $project = $this->project_model->get_data_project_all();
        $projectData = $this->utils->getFieldData($project,'title','id');

        $dataBannerSlide = $this->project_model->get_data_project_detail_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_project'] = $this->common->generateSelectOption($projectData,array(),$dataBannerSlide['project_id']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/project_detail/create', $data);
    }

    public function projectDetailDetail($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }
        $project = $this->project_model->get_data_project_all();
        $projectData = $this->utils->getFieldData($project,'title','id');

        $dataBannerSlide = $this->project_model->get_data_project_detail_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        $dataBannerSlide['project'] = $projectData[$dataBannerSlide['project_id']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/project_detail/detail', $data);
    }

    public function projectDetailList($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/project_detail/list', $data);
    }

    public function projectDetailSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->project_model->get_data_project_detail_all($title, $status, $status_approve);
        if (count($dataSlide) > 0) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'project-detail\',\''.$currentUser.'\',\''.$currentLanguage.'\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteProjectDetail(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region BUSINESS
    public function businessCreate($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_business)
            )
        );
        $this->load->view('admin/business/create', $data);
    }

    public function businessEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->business_model->get_data_business_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_position'] = $this->common->generateSelectOption($this->list_position_business, array(), $dataBannerSlide['position_order']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/business/create', $data);
    }

    public function businessDetail($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->business_model->get_data_business_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/business/detail', $data);
    }

    public function businessList($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/business/list', $data);
    }

    public function businessSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->business_model->get_data_business_all($title, $status, $status_approve);
        if (count($dataSlide) > 0) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'business\',\''.$currentUser.'\',\''.$currentLanguage.'\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteBusiness(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region NEWS
    public function newsCreate($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_type' => $this->common->generateSelectOption($this->list_type_news),
                'option_highlight' => $this->common->generateSelectOption($this->list_highlights)
            )
        );
        $this->load->view('admin/news/create', $data);
    }

    public function newsEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->news_model->get_data_news_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['option_type'] = $this->common->generateSelectOption($this->list_type_news, array(), $dataBannerSlide['type']);
        $dataBannerSlide['option_highlight'] = $this->common->generateSelectOption($this->list_highlights, array(), $dataBannerSlide['highlights']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/news/create', $data);
    }

    public function newsDetail($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->news_model->get_data_news_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        $dataBannerSlide['type'] = $this->list_type_news[$dataBannerSlide['type']];
        $dataBannerSlide['highlights'] = $this->list_highlights[$dataBannerSlide['highlights']];
        if ($dataBannerSlide['approver'] != '') {
            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
            if (count($userData) > 0 && $userData != false) {
                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/news/detail', $data);
    }

    public function newsList($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/news/list', $data);
    }

    public function newsSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->news_model->get_data_news_all($title, $status, $status_approve);
        if (count($dataSlide) > 0) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'news\',\''.$currentUser.'\',\''.$currentLanguage.'\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteNews(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $data[] = array(
                    'title' => $dataSlide[$i]['title'],
                    'title_vn' => $dataSlide[$i]['title_vn'],
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }

    public function newsSetting($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $listSetting = $this->news_model->get_all_news_setting();
        $listSetting = $this->utils->getFieldDataArray($listSetting,'');
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_type' => $this->common->generateSelectOption($this->list_type_news),
                'option_highlight' => $this->common->generateSelectOption($this->list_highlights),
                'data_setting' => $listSetting
            )
        );
        $this->load->view('admin/news/setting', $data);
    }
    #endregion

    #region INTRODUCE
    public function introduceCreate($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => array(
                'id' => null,
                'option_status' => $this->common->generateSelectOption($this->list_status_option, array('active', 'inactive')),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option, array('approved', 'denied')),
                'option_position' => $this->common->generateSelectOption($this->list_position_introduce)
            )
        );
        $this->load->view('admin/introduce/create', $data);
    }

    public function introduceEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model->get_data_introduce_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['banner_image'] = $this->domain . $dataBannerSlide['banner_image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce/create', $data);
    }

    public function introduceDetail($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model->get_data_introduce_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['image'] = $this->domain . $dataBannerSlide['image'];
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $dataBannerSlide['date_approved'] = $this->utils->convertDateTimeDisplay($dataBannerSlide['date_approved']);
        $dataBannerSlide['status_value'] = $dataBannerSlide['status'];
        $dataBannerSlide['status'] = $this->list_status_option[$dataBannerSlide['status']];
        $dataBannerSlide['status_approve_value'] = $dataBannerSlide['status_approve'];
        $dataBannerSlide['status_approve'] = $this->list_status_approve_option[$dataBannerSlide['status_approve']];
        if ($dataBannerSlide['approver'] != '') {
//            $userData = $this->main_model->get_info_user_with_id($dataBannerSlide['approver']);
//            if (count($userData) > 0 && $userData != false) {
//                $dataBannerSlide['approver'] = $userData['first_name'] . ' ' . $userData['last_name'];
//            }
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce/detail', $data);
    }

    public function introduceList($selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'search' => array(
                'option_status' => $this->common->generateSelectOption($this->list_status_option),
                'option_status_approve' => $this->common->generateSelectOption($this->list_status_approve_option)
            )
        );
        $this->load->view('admin/introduce/list', $data);
    }

    public function introduceSearch()
    {
        $parameters = $this->input->post('parameters');
        $currentUser = $parameters['current_user'];
        $currentLanguage = $parameters['current_language'];
        $title = $parameters['title'];
        $status = $parameters['status'];
        $status_approve = $parameters['status_approve'];

        $dataSlide = $this->introduce_model->get_data_introduce_all($title, $status, $status_approve);
        if (count($dataSlide) > 0) {
            $data = array();
            for ($i = 0; $i < count($dataSlide); $i++) {
                $button = '<button type="button" class="btn btn-info btn-circle" onclick="changeIframeToDetail(\'introduce\',\''.$currentUser.'\',\''.$currentLanguage.'\',\''.$dataSlide[$i]['id'].'\')">View Detail</button>';
                if($dataSlide[$i]['status_approve'] != 'approved'){
                    $button .= '<button type="button" class="btn red btn-circle" onclick="deleteintroduce(this,\''.$dataSlide[$i]['id'].'\')">Delete</button>';
                }
                $title = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce\',\''.$currentUser.'\',\''.$currentLanguage.'\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title'].'</a>';
                $title_vn = '<a href="javascript:void(0)" onclick="changeIframeToDetail(\'introduce\',\''.$currentUser.'\',\''.$currentLanguage.'\',\''.$dataSlide[$i]['id'].'\')">'.$dataSlide[$i]['title_vn'].'</a>';
                $data[] = array(
                    'title' => $title,
                    'title_vn' => $title_vn,
                    'status' => $this->list_status_option[$dataSlide[$i]['status']],
                    'status_approve' => $this->list_status_approve_option[$dataSlide[$i]['status_approve']],
                    'image' => '<img src="'.$this->domain.$dataSlide[$i]['image'].'" style="max-width: 135px; max-height: 80px;" />',
                    'action' => $button
                );
            }
            echo json_encode($data);
        } else {
            echo json_encode(array());
        }
    }
    #endregion

    #region INTRODUCE ABOUT
    public function introduceAboutEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_model->get_data_introduce_about_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about/create', $data);
    }
    #endregion

    #region INTRODUCE ABOUT AWARDS
    public function introduceAboutAwardsEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_awards_model->get_data_introduce_about_awards_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_awards/create', $data);
    }
    #endregion

    #region INTRODUCE ABOUT AWARDS ARTICLE
    public function introduceAboutAwardsArticleEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_awards_article_model->get_data_introduce_about_awards_article_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_awards_article/create', $data);
    }
    #endregion

    #region INTRODUCE ABOUT CORE
    public function introduceAboutCoreEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_core_model->get_data_introduce_about_core_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_core/create', $data);
    }
    #endregion

    #region INTRODUCE ABOUT SOCIAL
    public function introduceAboutSocialEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_social_model->get_data_introduce_about_social_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_social/create', $data);
    }
    #endregion

    #region INTRODUCE ABOUT SOCIAL
    public function introduceAboutSystemManagementEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_about_system_management_model->get_data_introduce_about_system_management_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_about_system_management/create', $data);
    }
    #endregion

    #region INTRODUCE MODEL CULTURE
    public function introduceModelCultureEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_culture_model->get_data_introduce_model_culture_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_culture/create', $data);
    }
    #endregion

    #region INTRODUCE MODEL STORIES
    public function introduceModelStoriesEdit($record, $selectLanguage, $user)
    {
        if (!isset($selectLanguage)) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $selectLanguage;
            $this->lang->load($selectLanguage, $selectLanguage);
        }

        $dataBannerSlide = $this->introduce_model_stories_model->get_data_introduce_model_stories_with_id($record);
        $dataBannerSlide['option_status'] = $this->common->generateSelectOption($this->list_status_option, array(), $dataBannerSlide['status']);
        $dataBannerSlide['option_status_approve'] = $this->common->generateSelectOption($this->list_status_approve_option, array(), $dataBannerSlide['status_approve']);
        $dataBannerSlide['date_display'] = $this->utils->convertDateDisplay($dataBannerSlide['date_display']);
        $data = array(
            'domain' => $this->domain,
            'language' => $this->lang->line('language'),
            'current_user' => $user,
            'info' => $dataBannerSlide
        );
        $this->load->view('admin/introduce_model_stories/create', $data);
    }
    #endregion
}
