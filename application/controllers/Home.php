<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Home extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL


        //LIBRARY
        $this->load->library('data');
        $this->load->library('checkdata');
    }

    public function index()
    {
        if (!isset($this->session->userdata['lang'])) {
            $this->session->set_userdata('lang', $this->config->item('language'));
            $this->currentLanguage = $this->config->item('language');
            $this->lang->load($this->config->item('language'), $this->config->item('language'));
        } else {
            $this->currentLanguage = $this->session->userdata['lang'];
            $this->lang->load($this->session->userdata['lang'], $this->session->userdata['lang']);
        }
        $data = $this->data->homePageInfo();
        $this->load->view('index', $data);
    }
}
