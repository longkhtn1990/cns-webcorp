<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class News extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->helper('url');
        //MODEL

        //LIBRARY
        $this->load->library('data');
        $this->load->library('checkdata');
    }

    public function show()
    {
        $data = $this->data->mainInfo();
        $data = $this->data->newsMenuInfo($data);
        $data['currentPage'] = 'news-media';
        $data = $this->data->newsContentInfo($data);
        $this->load->view('_partial/content/news/show', $data);
    }

    public function showList()
    {
        $data = $this->data->mainInfo();
        $data = $this->data->newsMenuInfo($data);
        $data['currentPage'] = 'news-media';
        $data = $this->data->newsContentInfo($data);
        $this->load->view('_partial/content/news/show', $data);
    }

    public function showDetail($page)
    {
        $data = $this->data->mainInfo();
        $data = $this->data->projectMenuInfo($data,$page);
        $data['currentPage'] = $page;
        $data = $this->data->projectDetailContentInfo($data,$page);
        $this->load->view('_partial/content/project/show-detail', $data);
    }


}
