<?php
/**
 * Created by PhpStorm.
 * User: harry.long
 * Date: 6/29/2017
 * Time: 9:49 AM
 */
class Utils
{
    function getFieldData($array, $field, $idField = null)
    {
        $_out = array();

        if (is_array($array)) {
            if ($idField == null) {
                foreach ($array as $value) {
                    $_out[] = trim($value[$field]);
                }
            } else {
                foreach ($array as $value) {
                    if(is_array($value[$field]) || is_object($value[$field])){
                        $_out[$value[$idField]] = $value[$field];
                    }else{
                        $_out[$value[$idField]] = trim($value[$field]);
                    }
                }
            }
            return $_out;
        } else {
            return false;
        }
    }

    function getFieldDataArray($array, $idField = null)
    {
        $_out = array();

        if (is_array($array)) {
            if ($idField == null) {
                foreach ($array as $value) {
                    $_out[] = trim($value);
                }
            } else {
                foreach ($array as $value) {
                    $_out[$value[$idField]] = $value;
                }
            }
            return $_out;
        } else {
            return false;
        }
    }

    function filterDocumentByCategory($documents = array()){
        $result = array();
        if(count($documents) > 0){
            for($i = 0;$i < count($documents);$i++){
                $result[$documents[$i]['category_id']][] = $documents[$i];
            }
        }
        return $result;
    }

    function saveImage($base64_string, $output_file)
    {
        // echo $output_file; die;
        $ifp = fopen($output_file, 'wb');
        fwrite($ifp, base64_decode($base64_string));
        fclose($ifp);
        return $output_file;
    }

    public function convertDateTimeToFormatDB($date_time)// d/m/Y h:ia
    {
        $date_time = trim($date_time);
        $str = explode(" ", $date_time);
        if (count($str) > 0 && count($str) == 2) {

            $date = $str[0];
            $time = $str[1];
            $result_date = self::convertDateDB($date);
            $result_time = self::convertTimeDB($time);
            return $result_date . ' ' . $result_time;
        } else {
            return '';
        }
    }

    public function convertDateDB($date)// d/m/Y
    {
        $str = explode("/", $date);
        $date_value = $str[0] . '-' . $str[1] . '-' . $str[2];
        if ($date_value == '' || $date_value == '0000-00-00') {
            return '';
        } else {
            return date('Y-m-d', strtotime($date_value));
        }
    }

    public function convertTimeDB($time)// h:ia
    {
        $time = trim($time);
        $str = explode(":", $time);
        $result = '';
        if (count($str) > 0 && count($str) == 2) {
            $hour = $str[0];
            $min_me = $str[1];
            if ($hour > 0 && $hour < 13) {
                if ($hour == 12) {
                    $hour = 0;
                }
                $me = substr($min_me, -2);
                $min = substr($min_me, 0, 2);
                if ($me == 'am') {
                    $result = $hour . ':' . $min . ':00';
                } else {
                    $result = ($hour + 12) . ':' . $min . ':00';
                }
                return $result;
            } else {
                return $result;
            }
        } else {
            return $result;
        }
    }

    public static function convertDateDisplay($date)
    {
        if ($date == '' || $date == '0000-00-00 00:00:00' || $date == '0000-00-00') {
            return '';
        } else {
            return date('d/m/Y', strtotime($date));
        }
    }

    public static function convertDateTimeDisplay($date_time)
    {
        if ($date_time == '' || $date_time == '0000-00-00 00:00:00' || $date_time == '0000-00-00') {
            return '';
        } else {
            return date('d/m/Y h:ia', strtotime($date_time));
        }
    }

    public static function convertDateToString($date){
        $str = explode("-", $date);
        $date_value = $str[0] . ' tháng ' . $str[1] . ' năm ' . $str[2];
        return $date_value;
    }
}