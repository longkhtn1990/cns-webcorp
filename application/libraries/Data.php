<?php

class Data
{
    private $domain = '';
    private $mainInfoData = array();

    public function mainInfo($page = '')
    {
        $data = array();
        $t = 0;
        $CI =& get_instance();
        $CI->load->model('main_model');
        $currentLanguage = $CI->session->userdata('language');
        $mainInfo = $CI->main_model->get_data_display();

        if ($mainInfo != false) {
            $this->domain = $mainInfo[$t]['base_domain'];
            $this->mainInfoData = $mainInfo[$t];

            $data['title'] = $currentLanguage == 'vietnamese' ? $mainInfo[$t]['title_vn'] : $mainInfo[$t]['title'];
            $data['favicon'] = $this->domain . $mainInfo[$t]['favicon'];
            $data['logo'] = $this->domain . $mainInfo[$t]['logo'];
            $data['link'] = $this->domain;
            $data['mainHeader'] = array(
                'home' => array(
                    'link' => $this->domain,
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_header_home_vn'] : $mainInfo[$t]['text_header_home']
                ),
                'member' => array(
                    'link' => $this->domain,
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_header_member_vn'] : $mainInfo[$t]['text_header_member']
                ),
                'customerContact' => array(
                    'link' => $this->domain,
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_header_contact_vn'] : $mainInfo[$t]['text_header_contact']
                ),
            );
            $data['mainFooter'] = array(
                'connecting' => array(
                    'title' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_connecting_footer_vn'] : $mainInfo[$t]['text_connecting_footer'],
                    'facebook' => $mainInfo[$t]['url_facebook'] != '' ? $mainInfo[$t]['url_facebook'] : '',
                    'youtube' => $mainInfo[$t]['url_youtube'] != '' ? $mainInfo[$t]['url_youtube'] : '',
                    'googlePlus' => $mainInfo[$t]['url_google_plus'] != '' ? $mainInfo[$t]['url_google_plus'] : '',
                    'totalView' => array(
                        'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_total_view_vn'] : $mainInfo[$t]['text_total_view'],
                        'number' => number_format($mainInfo[$t]['total_view']),
                        'unit' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['view_unit_vn'] : $mainInfo[$t]['view_unit'],
                    ),
                    'weekView' => array(
                        'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_total_view_week_vn'] : $mainInfo[$t]['text_total_view_week'],
                        'number' => number_format($mainInfo[$t]['total_view_week']),
                        'unit' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['view_unit_vn'] : $mainInfo[$t]['view_unit'],
                    ),
                ),
                'copyright' => array(
                    'pre-name' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['copyright_pre_text_vn'] : $mainInfo[$t]['copyright_pre_text'],
                    'name' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['copyright_text_vn'] : $mainInfo[$t]['copyright_text'],
                ),
                'career-job' => array(
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_career_job_vn'] : $mainInfo[$t]['text_career_job'],
                    'link' => ''
                ),
                'customer-contact' => array(
                    'text' => $currentLanguage == 'vietnamese' ? $mainInfo[$t]['text_customer_contact_vn'] : $mainInfo[$t]['text_customer_contact'],
                    'link' => ''
                ),
            );

        } else {
            $data = array(
                'title' => 'Tổng công ty công nghiệp Sài Gòn',
                'favicon' => base_url() . 'assets/images/favicon.ico',
                'logo' => base_url() . 'assets/images/logo.png',
                'link' => base_url()
            );
            $data['mainHeader'] = array(
                'home' => array(
                    'link' => base_url(),
                    'text' => 'Trang chủ'
                ),
                'member' => array(
                    'link' => base_url(),
                    'text' => 'Thành viên'
                ),
                'customerContact' => array(
                    'link' => base_url(),
                    'text' => 'Liên hệ'
                ),
            );
            $data['mainFooter'] = array(
                'connecting' => array(
                    'title' => 'Kết nối với chúng tôi',
                    'facebook' => '',
                    'youtube' => '',
                    'googlePlus' => '',
                    'totalView' => array(
                        'text' => 'Tổng lượt truy cập',
                        'number' => number_format(0),
                        'unit' => 'lượt'
                    ),
                    'weekView' => array(
                        'text' => 'Lượt truy cập trong tuần',
                        'number' => number_format(0),
                        'unit' => 'lượt'
                    ),
                ),
                'copyright' => array(
                    'pre-name' => 'Ủy ban Nhân dân TP.Hồ Chí Minh',
                    'name' => 'Tổng Công ty Công Nghiệp Sài Gòn - Trách nhiệm hữu hạn Một Thành Viên'
                ),
                'career-job' => array(
                    'text' => '',
                    'link' => ''
                ),
                'customer-contact' => array(
                    'text' => '',
                    'link' => ''
                ),
            );
        }
        $mainMenu = $CI->main_model->get_data_menu_display();
        if ($mainMenu != false) {
            for ($i = 0; $i < count($mainMenu); $i++) {
                $data['mainMenu'][$i]['text'] = $currentLanguage == 'vietnamese' ? $mainMenu[$i]['title_vn'] : $mainMenu[$i]['title'];
                if ($mainInfo[$t]['base_domain'] != '') {
                    $data['mainMenu'][$i]['link'] = $mainInfo[$t]['base_domain'] . $mainMenu[$i]['type'] . ($mainMenu[$i]['default'] != '' ? '/' . $mainMenu[$i]['default'] : '');
                    if ($page == $mainMenu[$i]['default'] && $page != '') {
                        $data['mainMenu'][$i]['active'] = true;
                    } else {
                        $data['mainMenu'][$i]['active'] = false;
                    }
                } else {
                    $data['mainMenu'][$i]['link'] = base_url() . $mainMenu[$i]['type'] . ($mainMenu[$i]['default'] != '' ? '/' . $mainMenu[$i]['title_vn'] : '');
                }
            }
        }
        /*$data['mainMenu'] = array(
            array(
                'text' => 'giới thiệu',
                'link' => base_url() . 'introduce/about-us',
                'active' => false
            ),
            array(
                'text' => 'lĩnh vực hoạt động',
                'link' => base_url() . 'business/technology-food',
                'active' => false
            ),
            array(
                'text' => 'dự án',
                'link' => base_url() . 'project/industrial-clusters',
                'active' => false
            ),
            array(
                'text' => 'khách hàng và đối tác',
                'link' => base_url() . 'customer-partner',
                'active' => false
            ),
            array(
                'text' => 'tin tức',
                'link' => base_url() . 'news-media',
                'active' => false
            ),
        );*/
        $data['mainFooter'] = array(
            'col-1' => array(
                'text' => 'Giới thiệu',
                'item' => array(
                    array(
                        'text' => 'Thông tin doanh nghiệp',
                        'link' => base_url() . 'introduce/about-us'
                    ),
                    array(
                        'text' => 'Thông tin doanh thu',
                        'link' => base_url() . 'introduce/about-revenue'
                    ),
                    array(
                        'text' => 'Thông điệp lãnh đạo',
                        'link' => base_url() . 'introduce/about-leadership'
                    ),
                    array(
                        'text' => 'Mô hình hoạt động',
                        'link' => base_url() . 'introduce/about-operational-model'
                    ),
                )
            ),
            'col-2' => array(
                'text' => 'Lĩnh vực kinh doanh',
                'item' => array(
                    array(
                        'text' => 'Công nghệ thực phẩm',
                        'link' => base_url() . 'business/food-technology'
                    ),
                    array(
                        'text' => 'Cơ khí - chế tạo máy',
                        'link' => base_url() . 'business/mechanical'
                    ),
                    array(
                        'text' => 'Hóa chất - cao su - nhựa',
                        'link' => base_url() . 'business/chemistry'
                    ),
                    array(
                        'text' => 'Điện tử - công nghệ thông tin',
                        'link' => base_url() . 'business/information-technology'
                    ),
                )
            ),
            'col-3' => array(
                'text' => 'Dự án',
                'item' => array(
                    array(
                        'text' => 'Dự Án Khu, Cụm Công Nghiệp',
                        'link' => base_url() . 'project/industrial-clusters'
                    ),
                    array(
                        'text' => 'Dự án công nghiệp - sản xuất',
                        'link' => base_url() . 'project/Industrial-production'
                    )
                )
            ),
            'col-4' => array(
                'text' => 'Dự án',
                'item' => array(
                    array(
                        'text' => 'Dự Án Khu, Cụm Công Nghiệp',
                        'link' => base_url() . 'project/industrial-clusters'
                    ),
                    array(
                        'text' => 'Dự án công nghiệp - sản xuất',
                        'link' => base_url() . 'project/Industrial-production'
                    )
                )
            ),
            'connecting' => array(
                'title' => 'Kết nối với chúng tôi',
                'facebook' => 'ss',
                'youtube' => 'ss',
                'googlePlus' => 'ss',
                'totalView' => array(
                    'text' => 'Tổng lượt truy cập',
                    'number' => number_format(56667),
                    'unit' => 'lượt'
                ),
                'weekView' => array(
                    'text' => 'Lượt truy cập trong tuần',
                    'number' => number_format(566),
                    'unit' => 'lượt'
                ),
            ),
            'copyright' => array(
                'pre-name' => 'Ủy ban Nhân dân TP.Hồ Chí Minh',
                'name' => 'Tổng Công ty Công Nghiệp Sài Gòn - Trách nhiệm hữu hạn Một Thành Viên'
            ),
            'career-job' => array(
                'text' => '',
                'link' => ''
            ),
            'customer-contact' => array(
                'text' => '',
                'link' => ''
            ),
        );
        return $data;
    }

    public function homePageInfo()
    {
        $data = self::mainInfo();
        $data['mainBanner'] = array();
        $CI =& get_instance();
        $CI->load->model('main_model');
        $CI->load->model('news_model');
        $currentLanguage = $CI->session->userdata('language');
        #region BANNER
        $mainBanner = $CI->main_model->get_data_banner_display();
        if ($mainBanner != false) {
            for ($i = 0; $i < count($mainBanner); $i++) {
                if ($currentLanguage == 'vietnamese') {
                    $title = $mainBanner[$i]['title_vn'];
                    $description = $mainBanner[$i]['description_vn'];
                } else {
                    $title = $mainBanner[$i]['title'];
                    $description = $mainBanner[$i]['description'];
                }
                $data['mainBanner'][] = array(
                    'image' => $this->domain . $mainBanner[$i]['image'],
                    'link' => $mainBanner[$i]['link_news'],
                    'title' => $title,
                    'description' => $description
                );
            }
        }
        #endregion

        #region BUSINESS
        $data['business'] = array();
        $mainBusiness = $CI->main_model->get_data_business_display();
        if ($mainBusiness != false) {
            if ($currentLanguage == 'vietnamese') {
                $data['business']['title'] = $this->mainInfoData['text_main_business_vn'];
            } else {
                $data['business']['title'] = $this->mainInfoData['text_main_business_vn'];
            }
            for ($i = 0; $i < count($mainBusiness); $i++) {
                if ($currentLanguage == 'vietnamese') {
                    $title = $mainBusiness[$i]['title_vn'];
                } else {
                    $title = $mainBusiness[$i]['title'];
                }
                $data['business']['item'][] = array(
                    'image' => $this->domain . $mainBusiness[$i]['image'],
                    'link' => 'business/' . $mainBusiness[$i]['alias'],
                    'text' => $title
                );
            }
        }
        #endregion

        $data['market'] = array(
            'display' => true,
            'title' => 'Thông tin thị trường',
            'main' => array(
                'link' => '',
                'image' => base_url() . '/assets/images/upload/news-item-2.jpg',
                'title' => 'Cao su và cọ dầu có “cứu” được HAG ?',
                'calender' => '24 tháng 5 năm 2017',
                'content' => 'Giá hàng hóa cơ bản, đặc biệt là giá cao su và cọ dầu đã ghi nhận sự hồi phục mạnh mẽ trong hơn 2 tháng qua. Với đóng góp vào hoạt động kinh doanh của CTCP Hoàng'
            ),
            'extra' => array(
                array(
                    'link' => '',
                    'title' => 'Tình hình giá cả thị trường tháng 10 và kế hoạch công tác tháng 11 năm 2016',
                    'calender' => '24 tháng 5 năm 2017',
                    'content' => 'Tình hình chung: Trên địa bàn tỉnh, tình hình cung cầu các mặt hàng trọng yếu tương đối ổn định. Chỉ số giá tiêu dùng trong tháng 10 tăng 0,15% so với tháng trước'
                ),
                array(
                    'link' => '',
                    'title' => 'Tình hình giá cả thị trường tháng 10 và kế hoạch công tác tháng 11 năm 2016',
                    'calender' => '24 tháng 5 năm 2017',
                    'content' => 'Tình hình chung: Trên địa bàn tỉnh, tình hình cung cầu các mặt hàng trọng yếu tương đối ổn định. Chỉ số giá tiêu dùng trong tháng 10 tăng 0,15% so với tháng trước'
                ),
                array(
                    'link' => '',
                    'title' => 'Tình hình giá cả thị trường tháng 10 và kế hoạch công tác tháng 11 năm 2016',
                    'calender' => '24 tháng 5 năm 2017',
                    'content' => 'Tình hình chung: Trên địa bàn tỉnh, tình hình cung cầu các mặt hàng trọng yếu tương đối ổn định. Chỉ số giá tiêu dùng trong tháng 10 tăng 0,15% so với tháng trước'
                ),
            ),
            'moreText' => 'Read more'
        );


        $data['typical'] = array(
            'display' => true,
            'title' => 'Tin Tức Tiêu Biểu',
            'main' => array(
                'link' => '',
                'image' => base_url() . '/assets/images/upload/news-item-1.jpg',
                'title' => 'Cao su và cọ dầu có “cứu” được HAG ?',
                'calender' => '24 tháng 5 năm 2017',
                'content' => 'Giá hàng hóa cơ bản, đặc biệt là giá cao su và cọ dầu đã ghi nhận sự hồi phục mạnh mẽ trong hơn 2 tháng qua. Với đóng góp vào hoạt động kinh doanh của CTCP Hoàng'
            ),
            'extra' => array(
                array(
                    'link' => '',
                    'title' => 'Tình hình giá cả thị trường tháng 10 và kế hoạch công tác tháng 11 năm 2016',
                    'calender' => '24 tháng 5 năm 2017',
                    'content' => 'Tình hình chung: Trên địa bàn tỉnh, tình hình cung cầu các mặt hàng trọng yếu tương đối ổn định. Chỉ số giá tiêu dùng trong tháng 10 tăng 0,15% so với tháng trước'
                ),
                array(
                    'link' => '',
                    'title' => 'Tình hình giá cả thị trường tháng 10 và kế hoạch công tác tháng 11 năm 2016',
                    'calender' => '24 tháng 5 năm 2017',
                    'content' => 'Tình hình chung: Trên địa bàn tỉnh, tình hình cung cầu các mặt hàng trọng yếu tương đối ổn định. Chỉ số giá tiêu dùng trong tháng 10 tăng 0,15% so với tháng trước'
                ),
                array(
                    'link' => '',
                    'title' => 'Tình hình giá cả thị trường tháng 10 và kế hoạch công tác tháng 11 năm 2016',
                    'calender' => '24 tháng 5 năm 2017',
                    'content' => 'Tình hình chung: Trên địa bàn tỉnh, tình hình cung cầu các mặt hàng trọng yếu tương đối ổn định. Chỉ số giá tiêu dùng trong tháng 10 tăng 0,15% so với tháng trước'
                ),
            ),
            'moreText' => 'Read more'
        );

        /*$highlights = $CI->news_model->get_data_highlights_news_display();
        if($highlights != false){
            $data['typical']['display'] = true;
            $data['typical']['main'] = array(
                'link' => $this->domain,
                'image' => base_url() . '/assets/images/upload/news-item-1.jpg',
                'title' => 'Cao su và cọ dầu có “cứu” được HAG ?',
                'calender' => '24 tháng 5 năm 2017',
                'content' => 'Giá hàng hóa cơ bản, đặc biệt là giá cao su và cọ dầu đã ghi nhận sự hồi phục mạnh mẽ trong hơn 2 tháng qua. Với đóng góp vào hoạt động kinh doanh của CTCP Hoàng'
            );
        }*/
        return $data;
    }

    ////////////////////// ABOUT ////////////////////
    public function aboutMenuInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('main_model');
        $currentLanguage = $CI->session->userdata('language');
        $menuData = $CI->main_model->get_data_introduce_display();
        if ($menuData != false) {
            $data['banner'] = array(
                'image' => base_url() . 'assets/images/upload/banner-about.jpg',
                'title' => 'Thông tin doanh nghiệp',
                'description' => 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên',
            );
            for ($i = 0; $i < count($menuData); $i++) {
                if ($currentLanguage == 'vietnamese') {
                    $title = $menuData[$i]['title_vn'];
                    $description = $menuData[$i]['description_vn'] != '' ? $menuData[$i]['description_vn'] : '';
                } else {
                    $title = $menuData[$i]['title'];
                    $description = $menuData[$i]['description'] != '' ? $menuData[$i]['description'] : '';
                }
                $data['menu'][$i] = array(
                    'text' => $title,
                    'link' => $this->domain . 'introduce/' . $menuData[$i]['alias'],
                    'active' => false,
                    'type' => $menuData[$i]['type'] != '' ? $menuData[$i]['type'] : 'about-us'
                );
                if ($page == $menuData[$i]['alias']) {
                    $data['menu'][$i]['active'] = true;
                    $data['currentType'] = $menuData[$i]['type'] != '' ? $menuData[$i]['type'] : 'about-us';
                    $data['banner'] = array(
                        'image' => $this->domain . $menuData[$i]['image'],
                        'title' => $title,
                        'description' => $description != '' ? $description : 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên'
                    );
                }
            }
        }
        return $data;
    }

    public function aboutUsInfo($data)
    {

        $CI =& get_instance();
        $CI->load->model('introduce_model');
        $currentLanguage = $CI->session->userdata('language');
        //ABOUT
        $dataAbout = $CI->introduce_model->get_data_about_display();
        if (count($dataAbout) > 0 && $dataAbout != false) {
            $data['content']['about']['title'] = $currentLanguage == 'vietnamese' ? $dataAbout['title_vn'] : $dataAbout['title'];
            $data['content']['about']['content'] = $currentLanguage == 'vietnamese' ? $dataAbout['content_vn'] : $dataAbout['content'];
        }
        //ABOUT CORE
        $dataAboutCore = $CI->introduce_model->get_data_about_core_display();
        if (count($dataAboutCore) > 0 && $dataAboutCore != false) {
            $data['content']['about-core']['title'] = $currentLanguage == 'vietnamese' ? $dataAboutCore['title_vn'] : $dataAboutCore['title'];
            if (count($dataAboutCore['articles']) > 0) {
                for ($i = 0; $i < count($dataAboutCore['articles']); $i++) {
                    $data['content']['about-core']['article-item'][$i]['title'] = $currentLanguage == 'vietnamese' ? $dataAboutCore['articles'][$i]['title_vn'] : $dataAboutCore['articles'][$i]['title'];
                    $data['content']['about-core']['article-item'][$i]['info'] = $currentLanguage == 'vietnamese' ? $dataAboutCore['articles'][$i]['content_vn'] : $dataAboutCore['articles'][$i]['content'];
                }
            }
        }
        //SOCIAL
        $dataAboutSocial = $CI->introduce_model->get_data_about_social_display();
        if (count($dataAboutSocial) > 0 && $dataAboutSocial != false) {
            $data['content']['social-responsibility']['title'] = $currentLanguage == 'vietnamese' ? $dataAboutSocial['title_vn'] : $dataAboutSocial['title'];
            $data['content']['social-responsibility']['content'] = $currentLanguage == 'vietnamese' ? $dataAboutSocial['content_vn'] : $dataAboutSocial['content'];
        }
        //OUR-NETWORK
        $dataAboutNetwork = $CI->introduce_model->get_data_about_our_network_display();
        if (count($dataAboutNetwork) && $dataAboutNetwork != false) {
            for ($i = 0; $i < count($dataAboutNetwork); $i++) {
                $data['content']['our-network'][] = array(
                    'title' => $currentLanguage == 'vietnamese' ? $dataAboutNetwork[$i]['title_vn'] : $dataAboutNetwork[$i]['title'],
                    'content' => $currentLanguage == 'vietnamese' ? $dataAboutNetwork[$i]['content_vn'] : $dataAboutNetwork[$i]['content'],
                    'item-1' => array(
                        'image' => $this->domain . $dataAboutNetwork[$i]['image_1'],
                        'number' => number_format($dataAboutNetwork[$i]['number_1']),
                        'text' => $currentLanguage == 'vietnamese' ? $dataAboutNetwork[$i]['text_1_vn'] : $dataAboutNetwork[$i]['text_1']
                    ),
                    'item-2' => array(
                        'image' => $this->domain . $dataAboutNetwork[$i]['image_2'],
                        'number' => number_format($dataAboutNetwork[$i]['number_2']),
                        'text' => $currentLanguage == 'vietnamese' ? $dataAboutNetwork[$i]['text_2_vn'] : $dataAboutNetwork[$i]['text_2']
                    )
                );
            }
        }
        //SYSTEM-MANAGEMENT
        $dataAboutSystem = $CI->introduce_model->get_data_about_system_management_display();
        if (count($dataAboutSystem) > 0 && $dataAboutSystem != false) {
            $data['content']['system-management']['title'] = $currentLanguage == 'vietnamese' ? $dataAboutSystem['title_vn'] : $dataAboutSystem['title'];
            $data['content']['system-management']['content'] = $currentLanguage == 'vietnamese' ? $dataAboutSystem['content_vn'] : $dataAboutSystem['content'];
        }
        //AWARDS
        $dataAboutAwards = $CI->introduce_model->get_data_about_awards_display();
        if (count($dataAboutAwards) > 0 && $dataAboutAwards != false) {
            $data['content']['awards']['title'] = $currentLanguage == 'vietnamese' ? $dataAboutAwards['title_vn'] : $dataAboutAwards['title'];
            if (count($dataAboutAwards['articles']) > 0) {
                for ($i = 0; $i < count($dataAboutAwards['articles']); $i++) {
                    $data['content']['awards']['items'][$i]['image'] = $this->domain . $dataAboutAwards['articles'][$i]['image'];
                    $data['content']['awards']['items'][$i]['title'] = $currentLanguage == 'vietnamese' ? $dataAboutAwards['articles'][$i]['title_vn'] : $dataAboutAwards['articles'][$i]['title'];
                    $data['content']['awards']['items'][$i]['content'] = $currentLanguage == 'vietnamese' ? $dataAboutAwards['articles'][$i]['content_vn'] : $dataAboutAwards['articles'][$i]['content'];
                }
            }
        }
        return $data;
    }

    public function aboutNewsInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('introduce_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataIntroduce = $CI->introduce_model->get_data_news_display($page);
        $data['content'] = array('news' => array('title' => '', 'content' => ''));
        if (count($dataIntroduce) > 0) {
            $data['content']['news']['title'] = $currentLanguage == 'vietnamese' ? $dataIntroduce['title_vn'] : $dataIntroduce['title'];
            $data['content']['news']['content'] = $currentLanguage == 'vietnamese' ? $dataIntroduce['content_vn'] : $dataIntroduce['content'];
        }
        return $data;
    }

    public function aboutModelInfo($data)
    {
        $CI =& get_instance();
        $CI->load->model('introduce_model');
        $currentLanguage = $CI->session->userdata('language');
        //CULTURE
        $dataCulture = $CI->introduce_model->get_data_model_culture_display();
        if (count($dataCulture) > 0 && $dataCulture != false) {
            $data['content']['culture']['title'] = $currentLanguage == 'vietnamese' ? $dataCulture['title_vn'] : $dataCulture['title'];
            $data['content']['culture']['content'] = $currentLanguage == 'vietnamese' ? $dataCulture['content_vn'] : $dataCulture['content'];
        }

        $dataStories = $CI->introduce_model->get_data_model_stories_display();
        if (count($dataStories) > 0 && $dataStories != false) {
            $data['content']['stories']['title'] = $currentLanguage == 'vietnamese' ? $dataStories['title_vn'] : $dataStories['title'];
            if (count($dataStories['member']) > 0) {
                for ($i = 0; $i < count($dataStories['member']); $i++) {
                    $data['content']['stories']['member'][$i]['image'] = $this->domain . $dataStories['member'][$i]['image'];
                    $data['content']['stories']['member'][$i]['name'] = $currentLanguage == 'vietnamese' ? $dataStories['member'][$i]['name_vn'] : $dataStories['member'][$i]['name'];
                    $data['content']['stories']['member'][$i]['position'] = $currentLanguage == 'vietnamese' ? $dataStories['member'][$i]['position_vn'] : $dataStories['member'][$i]['position'];
                    $data['content']['stories']['member'][$i]['info'] = $currentLanguage == 'vietnamese' ? $dataStories['member'][$i]['info_vn'] : $dataStories['member'][$i]['info'];
                }
            }
        }
        return $data;
    }
    ///////////////////// END ABOUT ////////////////////

    //////////////////// BUSINESS /////////////////////
    public function businessMenuInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('business_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataBusiness = $CI->business_model->get_data_business_display();

        if (count($dataBusiness) > 0 && $dataBusiness != false) {
            $data['banner'] = array(
                'image' => $this->domain . 'assets/images/upload/business-solution.jpg',
                'title' => ''
            );
            for ($i = 0; $i < count($dataBusiness); $i++) {
                $data['menu'][$i]['text'] = $currentLanguage == 'vietnamese' ? $dataBusiness[$i]['title_vn'] : $dataBusiness[$i]['title'];
                $data['menu'][$i]['link'] = $this->domain . 'business/' . $dataBusiness[$i]['alias'];
                if ($dataBusiness[$i]['alias'] == $page) {
                    $data['menu'][$i]['active'] = true;
                    $data['banner'] = array(
                        'image' => $this->domain . $dataBusiness[$i]['banner_image'],
                        'title' => $currentLanguage == 'vietnamese' ? $dataBusiness[$i]['title_vn'] : $dataBusiness[$i]['title']
                    );
                } else {
                    $data['menu'][$i]['active'] = false;
                }
            }
        }
        return $data;
    }

    public function businessContentInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('business_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataBusinessContent = $CI->business_model->get_data_business_content_display($page);

        if (count($dataBusinessContent) > 0 && $dataBusinessContent != false) {
            $data['content']['title'] = $currentLanguage == 'vietnamese' ? $dataBusinessContent['title_vn'] : $dataBusinessContent['title'];
            $data['content']['content'] = $currentLanguage == 'vietnamese' ? $dataBusinessContent['content_vn'] : $dataBusinessContent['content'];
        }

        return $data;
    }

    /////////////////// END BUSINESS //////////////////


    //////////////////// PROJECT //////////////////////
    public function projectMenuInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('project_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataProject = $CI->project_model->get_data_projects_display();

        if (count($dataProject) > 0 && $dataProject != false) {
            for ($i = 0; $i < count($dataProject); $i++) {
                $data['banner'] = array(
                    'image' => $this->domain . 'assets/images/upload/business-solution.jpg',
                    'title' => 'dự án',
                    'description' => 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên'
                );
                $data['menu'][$i]['text'] = $currentLanguage == 'vietnamese' ? $dataProject[$i]['title_vn'] : $dataProject[$i]['title'];
                $data['menu'][$i]['link'] = $this->domain . 'project/' . $dataProject[$i]['alias'];
                if ($dataProject[$i]['alias'] == $page) {
                    $data['menu'][$i]['active'] = true;
                    $data['banner'] = array(
                        'image' => $this->domain . $dataProject[$i]['banner_image'],
                        'title' => $currentLanguage == 'vietnamese' ? $dataProject[$i]['title_vn'] : $dataProject[$i]['title'],
                        'description' => $currentLanguage == 'vietnamese' ? $dataProject[$i]['description_vn'] : $dataProject[$i]['description']
                    );
                } else {
                    $data['menu'][$i]['active'] = false;
                }
            }
        }

        return $data;
    }

    public function projectDetailMenuInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('project_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataProject = $CI->project_model->get_data_projects_display();
        $dataPage = $CI->project_model->get_data_project_menu_detail_display($page);

        if (count($dataProject) > 0 && $dataProject != false) {
            for ($i = 0; $i < count($dataProject); $i++) {
                $data['banner'] = array(
                    'image' => $this->domain . 'assets/images/upload/business-solution.jpg',
                    'title' => 'dự án',
                    'description' => 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên'
                );
                $data['menu'][$i]['text'] = $currentLanguage == 'vietnamese' ? $dataProject[$i]['title_vn'] : $dataProject[$i]['title'];
                $data['menu'][$i]['link'] = $this->domain . 'project/' . $dataProject[$i]['alias'];
                if ($dataProject[$i]['alias'] == $dataPage['alias']) {
                    $data['menu'][$i]['active'] = true;
                    $data['banner'] = array(
                        'image' => $this->domain . $dataProject[$i]['banner_image'],
                        'title' => $currentLanguage == 'vietnamese' ? $dataProject[$i]['title_vn'] : $dataProject[$i]['title'],
                        'description' => $currentLanguage == 'vietnamese' ? $dataProject[$i]['description_vn'] : $dataProject[$i]['description']
                    );
                } else {
                    $data['menu'][$i]['active'] = false;
                }
            }
        }

        return $data;
    }

    public function projectContentInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('project_model');
        $CI->load->model('project_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataProjectList = $CI->project_model->get_data_project_list_display($page);
        if ($currentLanguage == 'vietnamese') {
            $CI->lang->load('vietnamese', 'vietnamese');
        } else {
            $CI->lang->load('english', 'english');
        }
        $language = $CI->lang->line('language');

        if (count($dataProjectList) > 0 && $dataProjectList != false) {
            $data['content']['title'] = $currentLanguage == 'vietnamese' ? $dataProjectList[0]['project_title_vn'] : $dataProjectList[0]['project_title'];
            for ($i = 0; $i < count($dataProjectList); $i++) {
                $data['content']['items'][$i] = array(
                    'day' => date('d', strtotime($dataProjectList[$i]['date_display'])),
                    'month' => date('m', strtotime($dataProjectList[$i]['date_display'])),
                    'year' => date('Y', strtotime($dataProjectList[$i]['date_display'])),
                    'image' => $this->domain . $dataProjectList[$i]['image'],
                    'name' => $currentLanguage == 'vietnamese' ? $dataProjectList[$i]['title_vn'] : $dataProjectList[$i]['title'],
                    'link' => $this->domain . 'project/detail/' . $dataProjectList[$i]['id'],
                    'text_button' => $language['button_view_detail']
                );
            }
        }
        return $data;
    }

    public function projectDetailContentInfo($data, $page)
    {
        $CI =& get_instance();
        $CI->load->model('project_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataProjectDetail = $CI->project_model->get_data_project_detail_display($page);
        if (count($dataProjectDetail) > 0 && $dataProjectDetail != false) {
            $data['content']['title'] = $currentLanguage == 'vietnamese' ? $dataProjectDetail['title_vn'] : $dataProjectDetail['title'];
            $data['content']['content'] = $currentLanguage == 'vietnamese' ? $dataProjectDetail['content_vn'] : $dataProjectDetail['content'];
        }
        return $data;
    }

    /////////////////// END PROJECT ///////////////////


    //////////////////// PARTNER /////////////////////
    public function partnerMenuInfo($data)
    {

        $CI =& get_instance();
        $CI->load->model('partner_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataPartner = $CI->partner_model->get_data_partner_banner_display();

        if (count($dataPartner) > 0 && $dataPartner != false) {
            $data['banner']['image'] = $this->domain . $dataPartner['image'];
            $data['banner']['title'] = $currentLanguage == 'vietnamese' ? $dataPartner['title_vn'] : $dataPartner['title'];
            $data['banner']['description'] = $currentLanguage == 'vietnamese' ? $dataPartner['description_vn'] : $dataPartner['description'];
        }
        return $data;
    }

    public function partnerContentInfo($data)
    {
        $data['content'] = array(
            'global-partnership' => array(
                'title' => 'Khách hàng và đối tác của CNS',
                'image' => 'assets/images/upload/global-partnership-img.jpg',
                'content' => '<p style="margin-bottom: 30px;">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>
                    <p style="margin-bottom: 30px;">
                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.
                    </p>'
            ),
            'customer' => array(
                'title' => 'Khách hàng',
                'items' => array(
                    'assets/images/upload/partner-1.png',
                    'assets/images/upload/partner-2.png',
                    'assets/images/upload/partner-3.png',
                    'assets/images/upload/partner-3.png',
                    'assets/images/upload/partner-3.png',
                    'assets/images/upload/partner-3.png',
                )
            ),
            'partner' => array(
                'title' => 'Đối tác',
                'items' => array(
                    'assets/images/upload/partner-1.png',
                    'assets/images/upload/partner-2.png',
                    'assets/images/upload/partner-3.png',
                    'assets/images/upload/partner-4.png',
                    'assets/images/upload/partner-4.png',
                    'assets/images/upload/partner-4.png',
                )
            )
        );

        $CI =& get_instance();
        $CI->load->model('partner_model');
        $currentLanguage = $CI->session->userdata('language');
        $dataPartnerGlobal = $CI->partner_model->get_data_partner_global_display();

        if (count($dataPartnerGlobal) > 0 && $dataPartnerGlobal != false) {
            $data['content']['global-partnership']['title'] = $currentLanguage == 'vietnamese' ? $dataPartnerGlobal['title_vn'] : $dataPartnerGlobal['title'];
            $data['content']['global-partnership']['image'] = $dataPartnerGlobal['image'];
            $data['content']['global-partnership']['content'] = $currentLanguage == 'vietnamese' ? $dataPartnerGlobal['description_vn'] : $dataPartnerGlobal['description'];
        }


        return $data;
    }

    /////////////////// END PARTNER //////////////////


    public function newsMenuInfo($data)
    {
        $data['banner'] = array(
            'image' => base_url() . 'assets/images/upload/theway-bg.jpg',
            'title' => 'Tin tức',
            'description' => 'Tổng Công ty Công Nghiệp Sài Gòn - TNHH Một Thành Viên'
        );
        return $data;
    }

    public function newsContentInfo($data)
    {

        $CI =& get_instance();
        $CI->load->model('news_model');
        $CI->load->library('utils');
        $currentLanguage = $CI->session->userdata('language');
        $list_category = $CI->news_model->get_list_category_display();
        $list_category = $CI->utils->getFieldDataArray($list_category, 'id');

        $data['content'] = array(
            'new-update' => array(
                'link' => '',
                'title' => '',
                'slogan' => ''
            ),
            'news-company' => array(
                'title' => '',
                'thumbnail' => array(),
                'list-news' => array(),
                'text-more' => $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more',
            ),
            'news-market' => array(
                'title' => '',
                'thumbnail' => array(),
                'list-news' => array(),
                'text-more' => $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more',
            ),
            'news-general' => array(
                'title' => '',
                'thumbnail' => array(),
                'list-news' => array(),
                'text-more' => $currentLanguage == 'vietnamese' ? 'Xem thêm' : 'Read more',
            ),
            'newspaper' => array(
                'title' => 'thông cáo báo chí',
                'list-news' => array(
                    array(
                        'title' => 'Báo cáo kết quả thực hiện sản xuất kinh doanh năm 2016 và ba (03) năm gần nhất',
                        'link' => ''
                    ), array(
                        'title' => 'Báo cáo kết quả thực hiện sản xuất kinh doanh năm 2016 và ba (03) năm gần nhất',
                        'link' => ''
                    ), array(
                        'title' => 'Báo cáo kết quả thực hiện sản xuất kinh doanh năm 2016 và ba (03) năm gần nhất',
                        'link' => ''
                    ), array(
                        'title' => 'Báo cáo kết quả thực hiện sản xuất kinh doanh năm 2016 và ba (03) năm gần nhất',
                        'link' => ''
                    ), array(
                        'title' => 'Báo cáo kết quả thực hiện sản xuất kinh doanh năm 2016 và ba (03) năm gần nhất',
                        'link' => ''
                    )
                ),
                'text-more' => 'Xem tất cả'
            ),
            'introduce' => array(
                'title' => 'Video giới thiệu CNS',
                'video-url' => 'https://www.youtube.com/embed/6byDRp7uf3Q',
                'list-news' => array(
                    array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    ), array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    ), array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    ), array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    ), array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    )
                ),
            ),
            'download-information' => array(
                'title' => 'Tải mẫu thông tin',
                'list-news' => array(
                    array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    ), array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    ), array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    ), array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    ), array(
                        'title' => 'Giới thiệu về Tổng Công ty công nghiệp Sài Gòn',
                        'link' => ''
                    )
                ),
                'text-more' => 'Xem tất cả'
            )
        );

        if ($list_category != false) {
            /*NEWS UPDATE*/
            if ($list_category['news_update']['status'] == 'active') {
                $latest_news = $CI->news_model->get_data_latest_news_display();
                if ($latest_news != false) {
                    $data['content']['new-update']['title'] = $currentLanguage == 'vietnamese' ? $latest_news['title_vn'] : $latest_news['title'];
                    $data['content']['new-update']['link'] = $this->domain . 'news-media/' . $latest_news['id'];
                    $data['content']['new-update']['slogan'] = $currentLanguage == 'vietnamese' ? 'Tin tức mới nhất' : 'News update';
                }
            }
            /*COMPANY*/
            if ($list_category['company']['status'] == 'active') {
                $company = $CI->news_model->get_date_company_news_display();
                if ($company != false) {
                    $data['content']['news-company']['title'] = $currentLanguage == 'vietnamese' ? $list_category['company']['title_vn'] : $list_category['company']['title'];
                    $data['content']['news-company']['thumbnail'] = array(
                        'link' => $this->domain . 'news-media/' . $company[0]['id'],
                        'image' => $this->domain . $company[0]['image'],
                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString($company[0]['date_display']) : date('d M Y',strtotime($company[0]['date_display'])),
                        'title' => $currentLanguage == 'vietnamese' ? $company[0]['title_vn'] : $company[0]['title'],
                        'info' => $currentLanguage == 'vietnamese' ? $company[0]['description_vn'] : $company[0]['description_vn'],
                    );
                    if (count($company) > 1) {
                        for ($i = 1; $i < count($company); $i++) {
                            $data['content']['news-company']['list-news'][] = array(
                                'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString($company[$i]['date_display']) : date('d M Y',strtotime($company[$i]['date_display'])),
                                'link' => $this->domain . 'news-media/' . $company[$i]['id'],
                                'title' => $currentLanguage == 'vietnamese' ? $company[$i]['title_vn'] : $company[$i]['title']
                            );
                        }
                    }else{
                        $data['content']['news-company']['list-news'] = array();
                    }
                }
            }
            /*MARKET*/
            if ($list_category['market']['status'] == 'active') {
                $market = $CI->news_model->get_date_market_news_display();
                if ($market != false) {
                    $data['content']['news-market']['title'] = $currentLanguage == 'vietnamese' ? $list_category['market']['title_vn'] : $list_category['market']['title'];
                    $data['content']['news-market']['thumbnail'] = array(
                        'link' => $this->domain . 'news-media/' . $market[0]['id'],
                        'image' => $this->domain . $market[0]['image'],
                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString($market[0]['date_display']) : date('d M Y',strtotime($market[0]['date_display'])),
                        'title' => $currentLanguage == 'vietnamese' ? $market[0]['title_vn'] : $market[0]['title'],
                        'info' => $currentLanguage == 'vietnamese' ? $market[0]['description_vn'] : $market[0]['description_vn'],
                    );
                    if (count($market) > 1) {
                        for ($i = 1; $i < count($market); $i++) {
                            $data['content']['news-market']['list-news'][] = array(
                                'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString($market[$i]['date_display']) : date('d M Y',strtotime($market[$i]['date_display'])),
                                'link' => $this->domain . 'news-media/' . $market[$i]['id'],
                                'title' => $currentLanguage == 'vietnamese' ? $market[$i]['title_vn'] : $market[$i]['title']
                            );
                        }
                    }else{
                        $data['content']['news-market']['list-news'] = array();
                    }
                }
            }
            /*GENERAL*/
            if ($list_category['general']['status'] == 'active') {
                $general = $CI->news_model->get_date_general_news_display();
                if ($general != false) {
                    $data['content']['news-market']['title'] = $currentLanguage == 'vietnamese' ? $list_category['general']['title_vn'] : $list_category['general']['title'];
                    $data['content']['news-market']['thumbnail'] = array(
                        'link' => $this->domain . 'news-media/' . $general[0]['id'],
                        'image' => $this->domain . $general[0]['image'],
                        'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString($general[0]['date_display']) : date('d M Y',strtotime($general[0]['date_display'])),
                        'title' => $currentLanguage == 'vietnamese' ? $general[0]['title_vn'] : $general[0]['title'],
                        'info' => $currentLanguage == 'vietnamese' ? $general[0]['description_vn'] : $general[0]['description_vn'],
                    );
                    if (count($general) > 1) {
                        for ($i = 1; $i < count($general); $i++) {
                            $data['content']['news-market']['list-news'][] = array(
                                'calender' => $currentLanguage == 'vietnamese' ? $CI->utils->convertDateToString($general[$i]['date_display']) : date('d M Y',strtotime($general[$i]['date_display'])),
                                'link' => $this->domain . 'news-media/' . $general[$i]['id'],
                                'title' => $currentLanguage == 'vietnamese' ? $general[$i]['title_vn'] : $general[$i]['title']
                            );
                        }
                    }else{
                        $data['content']['news-market']['list-news'] = array();
                    }
                }
            }
            /*NEWSPAPER*/
            if ($list_category['newspaper']['status'] == 'active') {
                $general = $CI->news_model->get_date_newspaper_news_display();
                if ($general != false) {
                    $data['content']['newspaper']['title'] = $currentLanguage == 'vietnamese' ? $list_category['general']['title_vn'] : $list_category['general']['title'];
                    if (count($general) > 0) {
                        for ($i = 0; $i < count($general); $i++) {
                            $data['content']['newspaper']['list-news'][] = array(
                                'link' => $this->domain . 'news-media/' . $general[$i]['id'],
                                'title' => $currentLanguage == 'vietnamese' ? $general[$i]['title_vn'] : $general[$i]['title']
                            );
                        }
                    }else{
                        $data['content']['news-market']['list-news'] = array();
                    }
                }
            }
            /*INTRODUCE*/
            if ($list_category['introduce']['status'] == 'active') {

            }
            /*DOWNLOAD*/
            if ($list_category['download-information']['status'] == 'active') {

            }
        }
        return $data;
    }


}