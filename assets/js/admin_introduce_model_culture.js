$(document).ready(function () {

    var e = $("#introduce_model_culture_form"), r = $(".alert-danger", e), i = $(".alert-success", e);
    e.validate({
        errorElement: "span",
        errorClass: "help-block help-block-error",
        focusInvalid: !1,
        ignore: "",
        messages: {},
        rules: {
            introduce_model_culture_title: {
                required: true
            },
            introduce_model_culture_title_vn: {
                required: true
            },
            introduce_model_culture_content: {
                required: true
            },
            introduce_model_culture_content_vn: {
                required: true
            },
            introduce_model_culture_status: {
                required: true
            },
            introduce_date_display: {
                required: true
            },
        },
        invalidHandler: function (e, t) {
            i.hide();
            r.show();
            App.scrollTo(r, -200);
        },
        errorPlacement: function (e, r) {
            r.is(":checkbox") ? e.insertAfter(r.closest(".md-checkbox-list, .md-checkbox-inline, .checkbox-list, .checkbox-inline")) : r.is(":radio") ? e.insertAfter(r.closest(".md-radio-list, .md-radio-inline, .radio-list,.radio-inline")) : e.insertAfter(r)
        },
        highlight: function (e) {
            $(e).closest(".form-group").addClass("has-error")
        },
        unhighlight: function (e) {
            $(e).closest(".form-group").removeClass("has-error");
        },
        success: function (e) {
            e.closest(".form-group").removeClass("has-error")
        },
        submitHandler: function (e) {
            i.show();
            r.hide();
            saveIntroduceModelCulture();
        }
    });
});

function saveIntroduceModelCulture() {
    var parameters = $('#introduce_model_culture_form').serializeArray();
    $.ajax({
        url: domain + '/admin/introduceModelCulture/save',
        type: 'post',
        data: {
            parameters: parameters
        },
        dataType: 'json',
        success: function (data) {
            spinner.hide();
            if (data['error_code'] == 1 && data['record'] != null && data['record'] != '') {
                // changeIframeToDetail('introduceAboutSystemManagement',$('#introduce_model_culture_current_user').val(),$('#introduce_model_culture_current_language').val(),data['record']);
                alert('save success');
            } else {
                alert('introduce about culture save error, pls check data.');
            }
        },
        error: function (data) {
            spinner.hide();
            alert('Introduce about culture save error, pls check data.');
        },
        beforeSend: function (jqXHR, setting) {
            spinner.show();
        }
    });
}