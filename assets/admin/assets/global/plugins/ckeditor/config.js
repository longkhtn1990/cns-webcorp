/**
 * @license Copyright (c) 2003-2014, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

var folder_ckfinder = location.protocol + "//" + location.host + '/assets/admin/assets/global/plugins/ckfinder';

CKEDITOR.editorConfig = function( config ) {
    // Define changes to default configuration here. For example:
    // config.language = 'fr';
    // config.uiColor = '#AADC6E';
    config.filebrowserBrowseUrl = folder_ckfinder+'/ckfinder.html';

    config.filebrowserImageBrowseUrl = folder_ckfinder+'/ckfinder.html?type=Images';

    config.filebrowserFlashBrowseUrl = folder_ckfinder+'/ckfinder.html?type=Flash';

    config.filebrowserUploadUrl = folder_ckfinder+'/core/connector/php/connector.php?command=QuickUpload&type=Files';

    config.filebrowserImageUploadUrl = folder_ckfinder+'/core/connector/php/connector.php?command=QuickUpload&type=Images';

    config.filebrowserFlashUploadUrl = folder_ckfinder+'/core/connector/php/connector.php?command=QuickUpload&type=Flash';

};